# societal

API intended to bring multiple social platforms into one single standard API. Enabling for easy implementation of new projects for easy platform integration in your applications.

## Why C#?

C# is a language that is very flexible both in a cross platform support kind of way and in the way that you write code. The API exposes interfaces which are implemented in the various sub projects making it easy to implement in console, web and UI applications, the intention is to make it as platform detached as possible so that it can be used in android and ios applications just as easy as in blazor or winforms or whatever type of project one intend to implement.

## API overlook

## Installation and usage

## Development

## Contribution guidelines

Full contribution guidelines can be found at https://jite.eu/2018/7/8/contribution-guidelines/

## License

 The MIT License (MIT)

Copyright © 2018 Jitesoft

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
